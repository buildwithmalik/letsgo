import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import APIService from './APIService';
import { Container, InputGroup, FormControl, ListGroup, Button } from 'react-bootstrap';

const SearchPage = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [destinations, setDestinations] = useState([]);
    const [groups, setGroups] = useState([]);
    const [users, setUsers] = useState([]);

    const navigate = useNavigate();

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
    };

    useEffect(() => {
        if (searchTerm) {
            setTimeout(async () => {
                const response = await APIService.search(searchTerm);
                setDestinations(response.destinations);
                setGroups(response.groups);
                setUsers(response.users);
            }, 500);
        }
    }, [searchTerm]);

    return (
        <Container>
            <InputGroup className="mb-3">
                <FormControl
                    type="text"
                    value={searchTerm}
                    onChange={handleSearchChange}
                    placeholder="Search..."
                    autoFocus
                />
                <Button variant="outline-secondary" className="ml-2">
                    <i className="fa fa-search"></i>
                </Button>
            </InputGroup>
            
            {searchTerm === '' ? (
                <p>Start searching! Or start typing to explore Blacksburg.</p>
            ) : (
                <>
                    <h4>Destinations</h4>
                    <ListGroup>
                        {destinations.length > 0 ? (
                            destinations.map((destination, index) => (
                                <ListGroup.Item key={index}>{destination.name}</ListGroup.Item>
                            ))
                        ) : (
                            <p>No destinations match your search term.</p>
                        )}
                    </ListGroup>

                    <h4 className="mt-4">Groups</h4>
                    <ListGroup>
                        {groups.length > 0 ? (
                            groups.map((group, index) => (
                                <ListGroup.Item key={index}>{group.group_name}</ListGroup.Item>
                            ))
                        ) : (
                            <p>No groups match your search term.</p>
                        )}
                    </ListGroup>

                    <h4 className="mt-4">Users</h4>
                    <ListGroup>
                        {users.length > 0 ? (
                            users.map((user, index) => (
                                <ListGroup.Item key={index}>{user.first_name} {user.last_name}</ListGroup.Item>
                            ))
                        ) : (
                            <p>No users match your search term.</p>
                        )}
                    </ListGroup>
                </>
            )}

            <Button className="mt-4" variant="secondary" onClick={() => navigate(-1)}>Go Back</Button>
        </Container>
    );
};

export default SearchPage;
